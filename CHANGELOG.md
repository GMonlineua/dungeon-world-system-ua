# 1.6.2

## Bug Fixes

- #101: Fixed an issue where toolbar icons in TinyMCE weren't visible when using the nightmode sheet.
- #106: Fixed an issue where the combat tracker didn't render correctly, which was preventing combatants from being added to it.
- #108: Updated the ASK dialog to use localized ability score names.
- Updated the spanish translation (thanks to @mickypardo)